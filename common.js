var msgself = false,
	msgfn = null,
	msgcallback,
	callback_group = {};
function manage_msg(msg) {
	if (typeof msg == "object") {
		if (msg.type === "fn") {
			var _fn = msg.fn,
				type = $.type(_fn);
			var _pram = msg.pram || null;
			var ptype = $.type(_pram);
			msgfn = _fn; //记录现在调用的fn名，POST callback时带上
			msgcallback = msg.callback || null;
			if (!msgself && type == "string") {
				_pram ? eval(_fn)[ptype !== 'array' ? 'call' : 'apply'](window, _pram) : eval(_fn).call(window);
			}
		}
	} else if (typeof msg === "string") {
		if (msg.indexOf("{") >= 0) {
			return JSON.parse(msg)
		} else {
			console.log(msg)
			switch (msg) {
				case "hiback":
					window.history.back();
					break;
				case "hiforward":
					window.history.forward();
					break;
				case "reload":
					window.location.reload()
					break;
				case "notfiy":
					startPlay();
					break;
			}
		};
	}
}

function initpost() {
	top.postMessage('hi,appinit', "*");
}

function push_callback(opt) {
	callback_group[opt.fn] = {
		'error': opt.error,
		'success': opt.success
	}
}

function manage_callback(opt) {
	var _errmsg = opt.error,
		_success = opt.success,
		_fn = opt.fn;
	if (callback_group[_fn]) {
		_errmsg ? callback_group[_fn].error(_errmsg) : callback_group[_fn].success(_success);
	}
}

function post_callback(opt) {
	var _opt = opt;
	_opt.fn = msgfn;
	postmsg("manage_callback", _opt)
}

function postmsg(msg, pram, callback) {
	var _msg = msg;
	if (typeof msg === "string") {
		var toarr = _msg.indexOf("()") >= 0;
		if (toarr || pram) {
			var msgobj = {};
			msgobj.fn = toarr ? _msg.split('()')[0] : _msg;
			msgobj.type = "fn";
			pram && (msgobj.pram = pram);
			_msg = msgobj;
		}
		if (callback) {
			_msg.callback = true;
			callback.fn = _msg.fn;
			push_callback(callback);
		}
		top.postMessage(_msg, "*");
		console.log(_msg);
	}
}
window.addEventListener("message", function(e) {
	console.log("for parntwin msg:", e.data);
	if (e.data === "hi,init") {
		!window.parentwin && (window.parentwin = e.source);
		postmsg("hi,appinit");
	} else {
		manage_msg(e.data);
	}
}, false)
initpost()

function handQuery(token) {
	postmsg('opencef', token, {
		'error': function(msg) {
			console.log('出错了', msg)
		},
		'success': function(msg) {
			console.log('易机启动成功', msg)
		}
	});
}

function sendtoken() {
	call_handheld(handQuery)
}